
export default {
    MONGO_HOST: process.env.MONGO_HOST,
    HOST: process.env.HOST,
    PORT: process.env.PORT
}