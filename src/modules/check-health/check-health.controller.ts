import { Request, Response } from "express";
import { ResponseCode } from "../../common/consts"
import { Message } from "../../common/consts";

export const CheckHealthController = (req: Request, res: Response) => {
    return res.status(ResponseCode.OK).json({
        statusCode: ResponseCode.OK,
        message: Message.OK,
        data: []
    })
}
