import { UserDocument } from "../../common/interface/user.interface";
import User from "./user.schema";
import { Document } from "mongoose";

export const getAllRepository = () => User.find({});

export const createRepository = (body: UserDocument) => User.create(body);