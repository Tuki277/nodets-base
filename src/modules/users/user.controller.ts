import { ResponseCode } from "../../common/consts"
import { Request, Response } from "express";
import { getAllUserService } from "./user.service";
import { MESSAGE } from "../../common/interface/response.interface";

export const getAllUser = async (req: Request, res: Response) => {
    const data = await getAllUserService();
    return res.status(ResponseCode.OK).json({
        statusCode: ResponseCode.OK,
        message: MESSAGE.OK,
        data
    })
}