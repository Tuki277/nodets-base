import { UserDocument } from "../../common/interface/user.interface";
import { createRepository, getAllRepository } from "./user.repository";
import User from "./user.schema";

export const getAllUserService = async () => {
    try {
        return await getAllRepository();
    } catch (error: any) {
        throw new Error(error.message);
    }
}

export const create = async (body: UserDocument) => {
    try {
        return await createRepository(body);
    } catch (error: any) {
        throw new Error(error.message);
    }
}