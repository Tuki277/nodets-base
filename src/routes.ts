import { Express } from "express";
import { CheckHealthController } from "./modules/check-health/check-health.controller";
import { getAllUser } from "./modules/users/user.controller";

export default function (app: Express) {
    app.get('/check-health', CheckHealthController);

    app.get('/user', getAllUser);
    app.post('/user', CheckHealthController);
}