import mongoose from "mongoose";
import log from "../logger";
import config from 'config';

const mongoDB = async () => {

    const mongoURL = config.get("MONGO_HOST") as string;

    try {
        await mongoose.connect(mongoURL, {
            // useNewUrlParser: true, // <-- no longer necessary
        })
        log.info(`⚡️ Mongodb: ${process.env.MONGO_HOST} `)
    } catch (error) {
        log.info(error)
        process.exit(1)
    }
}

export default mongoDB;