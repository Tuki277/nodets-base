
import express from 'express'
import log from './logger'
import cors from 'cors'
import routes from './routes';
import mongoDB from './database/mongo';
import config from 'config';

const app = express();

app.use(cors())

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const port = config.get("PORT") as number;
const host = config.get("HOST") as string;

app.listen(port, host, () => {
    log.info('=========================================================');
    log.info(`⚡️ Server listing at http://${host}:${port}`);
    log.info('=========================================================');
    routes(app);
    mongoDB();
})