import mongoose from "mongoose";

export interface DateTime {
    createdAt: Date;
    updatedAt: Date;
}

export interface UserDocument extends mongoose.Document, DateTime {
    Username: string;
    FullName: string;
    PhoneNumber: string;
    Password: string;
    Gender: number; // 1: Male - 2: Female
    Rule: number;
    comparePassword(candidatePassword: string): Promise<boolean>
}