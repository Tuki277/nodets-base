export enum ResponseCode {
    OK = 200,
    CREATED = 201,
    ACCEPTED = 202,
    NO_CONTENT = 204,
    VALIDATION_FAILED = 400,
    UNAUTHORIZED = 401,
    FORBIDDEN = 403,
    NOT_FOUND = 404,
    METHOD_NOT_ALLOWED = 405,
    CONFLICT = 409,
    INTERNAL_SERVER_ERROR = 500,
    GATEWAY_TIMEOUT = 504
}

export enum ErrorCode {
    INVALID_PARAM = 'Invalid parameter',
    CONFLICT = 'Conflict',
    RESOURCE_NOT_FOUND = 'Resource not found',
    UNAUTHORIZED = 'Unauthorized',
    FORBIDDEN = 'Forbidden'
};

export enum MESSAGE {
    OK = "OK",
    NOT_FOUND = "Not found",
    BAD_REQUEST = "Bad request",
}

export interface IResponse<T> {
    statusCode: ResponseCode,
    message: MESSAGE,
    data: T,
}